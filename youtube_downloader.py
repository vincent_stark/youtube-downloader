#!/usr/bin/env python
"""This script downloads YouTube media files and allows
to choose their quality"""

import locale
import re
from sys import stdout
from urllib import urlopen, unquote, urlretrieve
from urlparse import parse_qs

# Set locale for digit grouping
locale.setlocale(locale.LC_ALL, "")

def _reporthook(numblocks, blocksize, filesize):
    """Download progress hook"""

    filesize_grp = locale.format("%i", filesize, True)
    output = "\r{0:5.1f}% of {1:s} bytes". \
        format(min(100, float(numblocks * blocksize) / filesize * 100), \
               filesize_grp)
    stdout.write(output)
    stdout.flush()

def geturl(url, dst):
    """YouTube Video download function"""
    print("\nSaving video to '%s'" % (dst) )
    if stdout.isatty():
        return urlretrieve(url, dst, lambda nb, bs, fs, \
                           url=url: _reporthook(nb, bs, fs))
    else:
        return urlretrieve(url, dst)

# Ask for Video ID
video_id = raw_input("\nYouTube Video ID? ")
if not re.match(r'[A-Za-z0-9\-]+', video_id):
    raise ValueError("Video ID should contain alphanumeric characters only")

# Retrieve Video Information from YouTube
video_info = parse_qs(
               unquote(
                 urlopen("http://www.youtube.com/get_video_info?video_id=" + \
                         video_id).read()
               )
             )

if video_info["status"] == ["fail"]:
    raise StandardError("Invalid Video ID")

# Prepare a list of video formats
video_fmt = "".join(video_info["fmt_list"]).split(",")

# Remove a non-functional format
video_fmt.remove("5/320x240/7/0/0")

# Sort format numbers
video_fmt.sort(key=lambda x: int(x.split("/")[0]))

# Ask for preferred format
print("\nPlease choose a video format:\n")
for fmt in video_fmt:
    print("%2s: %9s %3s %3s %4s" % tuple([val for val in fmt.split("/")]))

fmt_chosen = raw_input("\nFormat? ")
if fmt_chosen not in [fmt.split("/")[0] for fmt in video_fmt]:
    raise ValueError("You must select from available formats only")

# Video Download
video_link = "".join(
                [itag for itag in video_info["itag"] \
                 if itag.split(",")[0] == fmt_chosen] \
             ).split(",")[1].split("url=")[1]

geturl(video_link, fmt_chosen + "-" + video_id + ".mp4")

print("\n")
